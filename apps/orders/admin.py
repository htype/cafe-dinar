from django.contrib import admin

from orders.models import Client, Order, OrderItem
from orders.forms import ClientAdminModelForm, OrderAdminModelForm
from orders.forms import OrderItemAdminModelForm


class ClientAdminModel(admin.ModelAdmin):
    form = ClientAdminModelForm


class OrderAdminModel(admin.ModelAdmin):
    form = OrderAdminModelForm

class OrderItemAdminModel(admin.ModelAdmin):
    form = OrderItemAdminModelForm

admin.site.register(Client, ClientAdminModel)
admin.site.register(Order, OrderAdminModel)
admin.site.register(OrderItem, OrderItemAdminModel)
