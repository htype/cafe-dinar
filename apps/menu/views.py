from django.views.generic.base import TemplateView

from menu.models import Category
from orders.models import Order, OrderItem

class HomePageView(TemplateView):
    template_name = u"menu/index.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        user = self.request.user
        if user.is_authenticated and hasattr(user, "client"):
            client_order, created = Order.objects.get_or_create(
                client = user.client,
                status = Order.STATUS_BASKET,
            )
        else:
            order_pk = self.request.session.get("order_pk", 0)
            client_order, created = Order.objects.get_or_create(
                pk = order_pk,
                status = Order.STATUS_BASKET
            )

        order_items = OrderItem.objects.filter(order = client_order)
        categories = Category.objects.filter(dish__isnull=False).distinct()
        basket_items = [{
            "dish_name": item.dish.name,
            "quantity": item.quantity,
            "price": item.quantity * item.dish.cost,
        } for item in order_items]
        in_total = reduce(lambda acc, x: acc + x["price"], basket_items, 0)

        context["basket_items"] = basket_items
        context["in_total"] = in_total
        context["categories"] = categories
        context["order_pk"] = client_order.pk
        return context
