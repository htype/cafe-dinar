from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic.detail import View
from django.views.generic.detail import DetailView
from django.http import JsonResponse
from django.db.models import Sum, F

from menu.models import Dish
from orders.models import Order, OrderItem

class JsonResponseMixin(object):
    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(context, safe=False)

class BasketDeleteAll(JsonResponseMixin, View):
    def post(self, request, *args, **kwargs):
        action = request.POST.get("action")
        if action != "delete all":
            return send_status(self, False)

        user = request.user
        if (user.is_authenticated and hasattr(user, "client")):
            client_order = Order.objects.get(
                client = user.client,
                status = Order.STATUS_BASKET,
            )
        else:
            order_pk = request.session.get("order_pk")
            client_order = Order.objects.get(pk = order_pk)

        if client_order:
            OrderItem.objects.filter(order = client_order).delete()
            client_order.cost = 0
            return send_status(self, True)
        return send_status(self, False)


class BasketUpdateItem(JsonResponseMixin, View):
    model = Dish

    def get_cart_item(self, order, dish):
        try:
            order_item = OrderItem.objects.get(
                order  = order,
                dish   = dish,
            )
        except ObjectDoesNotExist:
            return None
        return order_item

    def form_data_to_send(self, dish_name, dish_quantity, dish_cost):
        basket_item = {
            "dish_name": dish_name,
            "quantity": dish_quantity,
            "price": dish_quantity * dish_cost,
        }
        status_dictionary = get_status_dictionary(True)
        basket_item.update(status_dictionary)
        return basket_item

    def getOrder(self, request):
        user = request.user
        if (user.is_authenticated and hasattr(user, "client")):
            order, created = Order.objects.get_or_create(
                client = user.client,
                status = Order.STATUS_BASKET,
            )
            return order
        else:
            order_pk = request.session.get("order_pk", None)
            if not order_pk:
                new_order = Order.objects.create(status = Order.STATUS_BASKET)
                request.session["order_pk"] = new_order.pk
                return new_order
            return Order.objects.get(pk = order_pk)

    def create_cart_item(self, order, dish):
        return OrderItem.objects.create(
            order = order,
            dish = dish,
            quantity = 1
        )

    def update_cart_item(self, cart_item, request):
        quantity = request.POST.get("quantity", 0)
        if not quantity:
            return None
        cart_item.quantity = int(quantity)
        cart_item.save()
        return cart_item

    def delete_cart_item(self, cart_item):
        OrderItem.objects.filter(pk = cart_item.pk).delete()
        return cart_item

    def post(self, request, *args, **kwargs):
        dish_name = request.POST.get("name")
        action = request.POST.get("action")
        dish = Dish.objects.get(name = dish_name)
        if not dish:
            return send_status(self, False)

        user = request.user
        order = self.getOrder(request)
        cart_item = self.get_cart_item(order, dish)

        updated_item = None
        if (action == "create" and not cart_item):
            updated_item = self.create_cart_item(order, dish)
        elif (action == "update" and cart_item):
            updated_item = self.update_cart_item(cart_item, request)
        elif (action == "delete" and cart_item):
            updated_item = self.delete_cart_item(cart_item)

        if (updated_item):
            data_to_send = self.form_data_to_send(
                dish.name,
                updated_item.quantity,
                dish.cost,
            )
            recalculate_in_total(order)
            return self.render_to_json_response(data_to_send)

        return send_status(self, False)


def get_status_dictionary(is_success):
    status = "success" if is_success else "fail"
    return {"status": status}

def send_status(view, is_success):
    status_dict = get_status_dictionary(is_success)
    return view.render_to_json_response(status_dict)

def recalculate_in_total(order):
    sum = OrderItem.objects.filter(order = order).aggregate(
        in_total = Sum(F("dish__cost") * F("quantity"))
    )
    order.cost = sum["in_total"]
    order.save()

class ChequeView(DetailView):
    model = Order
    template_name = u"orders/cheque.html"

    def get_context_data(self, **kwargs):
        context = super(ChequeView, self).get_context_data(**kwargs)
        order = self.object
        order_items = OrderItem.objects.filter(order = order)
        items = [{
            "name": item.dish.name,
            "quantity": item.quantity,
            "cost": item.dish.cost,
            "sum": item.dish.cost * item.quantity,
        } for item in order_items]
        context["items"] = items
        return context
