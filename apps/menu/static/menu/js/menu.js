(function(){

    function formDishDictionary(name, action, quantity=0) {
        var dishDictionary = {
            "name": name,
            "action": action,
        };
        if (quantity) {
            dishDictionary["quantity"] = quantity;
        }
        return dishDictionary;
    }

    function sendDictDataViaPost(url, dictData, success) {
        var formData = $("form[name='menu']").serialize();
        var dataToSend = formData + '&' + $.param(dictData);
        $.post(
            url,
            dataToSend,
            success
        );
    }


    function getOrCreateDishInOrder(basketItem) {
        var dishRowSelector = "[name='" + basketItem.dish_name + "']";
        var dishRows = $(dishRowSelector);
        if (dishRows.length == 1)
            return [dishRows[0], false];

        if (dishRows.length == 0) {
            var trSelector = ".table tbody tr";
            var lastChildTrSelector = trSelector + ":last-child";
            var lastDishNumber = $(trSelector).length;
            var basketItemHtml = "<tr class='basket-item' name='" + basketItem.dish_name + "'>" +
                                    "<th scope='row'>" + lastDishNumber + "</th>" +
                                    "<td class='basket-item-name'>" + basketItem.dish_name + "</td>" +
                                    "<td class='basket-item-quantity'>" +
                                        "<input type='number' min='1' value='" + basketItem.quantity + "'>" +
                                    "</td>" +
                                    "<td class='basket-item-price'>" + basketItem.price + "</td>" +
                                    "<td class='remove-dish-button'>&#10006;</td>" +
                                 "</tr>";
            var newBasketItem = $(basketItemHtml).insertBefore(lastChildTrSelector);

            $(".basket-item").on("input", updateDishQuantity);
            $(newBasketItem).find('.remove-dish-button').on("click", removeDishFromBasket);
            return [newBasketItem, true];
        }
    }

    function updateDishInOrder(basketItem, updateQuantity = true) {
        var [dishRow, isCreated] = getOrCreateDishInOrder(basketItem);
        if (!isCreated) {
            if (updateQuantity) {
                $(dishRow).find(".basket-item-quantity input").val(basketItem.quantity);
            }
            $(dishRow).find(".basket-item-price").text(basketItem.price);
        }
    }

    function updateTotalSum() {
        var prices = $(".basket-item-price");
        var inTotal = 0;
        prices.each(function(cur) {
            inTotal += parseInt($(this).text());
        });
        var inTotalSelector = ".basket-in-total";
        inTotalSelector = $(inTotalSelector).text(inTotal);
    }

    function updateDishNumbers() {
        var counter = 1;
        $(".basket-item th").each(function(){
            $(this).text(counter);
            counter++;
        });
    }

    function setNoItemPlaceholder() {
        var placeholder = "<tr class='basket-no-item'> <th></th> <th colspan='3'>There are no items in the order.<th> </tr>"
        var trSelector = ".table tbody tr";
        var lastChildTrSelector = trSelector + ":last-child";
        $(placeholder).insertBefore(lastChildTrSelector);
    }

    function removeDishFromBasket() {
        var dishRow = $(this).parent();
        var dishName = dishRow.find(".basket-item-name").text().trim();
        var dishDict = formDishDictionary(dishName, "delete");
        sendDictDataViaPost("orders/basket/update-item/", dishDict, function(basketItem) {
            if (basketItem["status"] == "success") {
                dishRow.remove();
                updateDishNumbers();
                updateTotalSum();
                if ($(".basket-item").length == 0) {
                    setNoItemPlaceholder();
                }
            }
        });
    }

    function removeAllDishFromBasket() {
        var actionDict = {
            "action": "delete all"
        };
        sendDictDataViaPost("orders/basket/delete-all/", actionDict, function(basketItem) {
            if (basketItem["status"] == "success") {
                $(".basket-item").remove();
                updateTotalSum();
                if ($(".basket-no-item").length == 0) {
                    setNoItemPlaceholder();
                }
            }
        });

    }

    function updateDishQuantity() {
        var dishName = $(this).find(".basket-item-name").text().trim();
        var quantity = parseInt($(this).find(".basket-item-quantity input").val());
        var dishDict = formDishDictionary(dishName, "update", quantity);
        sendDictDataViaPost("orders/basket/update-item/", dishDict, function(basketItem) {
            if (basketItem["status"] == "success") {
                updateDishInOrder(basketItem, false);
                updateTotalSum();
            }
        });
    }

    $(document).ready(function(){

        $(".basket-item").on("input", updateDishQuantity);

        $(".remove-dish-button").on("click", removeDishFromBasket);

        $(".calc-button-clean").on("click", removeAllDishFromBasket);

        $(".item").click(function(){
            var dishName = $(this).find(".item-description").text().trim();
            var dishRowSelector = "tr[name='" + dishName + "']";
            var dishRows = $(dishRowSelector);
            var dishDict = {};
            if (dishRows.length > 0) {
                var quantity = parseInt($(dishRows[0]).find(".basket-item-quantity input").val());
                dishDict = formDishDictionary(dishName, "update", quantity + 1);
            } else {
                dishDict = formDishDictionary(dishName, "create");
            }
            sendDictDataViaPost("orders/basket/update-item/", dishDict, function(basketItem) {
                if (basketItem["status"] == "success") {
                    if ($(".basket-no-item")) {
                        $(".basket-no-item").remove();
                    }
                    updateDishInOrder(basketItem);
                    updateTotalSum();
                }
            });
        });

    });
})();
