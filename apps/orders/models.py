#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from menu.models import Dish

class Client(models.Model):
    class Meta:
        verbose_name = u"Клиент"
        verbose_name_plural = u"Клиенты"

    user = models.OneToOneField(User, verbose_name=u"пользователь", on_delete=models.CASCADE)

    def __unicode__(self):
        return self.user.username

class Order(models.Model):
    class Meta:
        verbose_name = u"Заказ"
        verbose_name_plural = u"Заказы"

    STATUS_USUAL = "SU"
    STATUS_BASKET = "SB"
    STATUSES = (
        (STATUS_USUAL, "usual order"),
        (STATUS_BASKET, "basket"),
    )

    client = models.ForeignKey(Client, verbose_name=u"клиент",
        on_delete=models.CASCADE, null=True, blank=True)
    dishes = models.ManyToManyField(
        Dish,
        through="OrderItem",
        through_fields=("order", "dish"),
    )
    date = models.DateTimeField(verbose_name=u"дата", auto_now_add=True)
    cost = models.IntegerField(verbose_name=u"полная_стоимость", null=True, default=0)
    status = models.CharField(
        verbose_name=u"статус",
        max_length=2,
        choices=STATUSES,
        default=STATUS_USUAL,
    )

    def __unicode__(self):
        return "Order #{}".format(self.pk)

class OrderItem(models.Model):
    class Meta:
        verbose_name = u"Пункт заказа"
        verbose_name_plural = u"Пункты заказа"

    order = models.ForeignKey(Order, verbose_name=u"заказ", on_delete=models.CASCADE)
    dish = models.ForeignKey(Dish, verbose_name=u"блюдо", on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(verbose_name=u"количество_блюд", default=0)

    def __unicode__(self):
        return u"{} for order #{}".format(self.dish.name, self.order.pk)
