# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-19 10:38
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20160915_1149'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': '\u041a\u043b\u0438\u0435\u043d\u0442', 'verbose_name_plural': '\u041a\u043b\u0438\u0435\u043d\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': '\u0417\u0430\u043a\u0430\u0437', 'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b'},
        ),
        migrations.AlterModelOptions(
            name='orderitem',
            options={'verbose_name': '\u041f\u0443\u043d\u043a\u0442 \u0437\u0430\u043a\u0430\u0437\u0430', 'verbose_name_plural': '\u041f\u0443\u043d\u043a\u0442\u044b \u0437\u0430\u043a\u0430\u0437\u0430'},
        ),
    ]
