from django.contrib import admin

from menu.models import Category, Dish
from menu.forms import DishAdminModelForm

class DishAdminModel(admin.ModelAdmin):
    form = DishAdminModelForm

admin.site.register(Category)
admin.site.register(Dish, DishAdminModel)
