import datetime
import calendar

from django.shortcuts import render
from django.views.generic.list import ListView
from django.db.models import Sum, F
from django.utils.decorators import method_decorator
from django.contrib.admin.views.decorators import staff_member_required

from orders.models import Order, OrderItem
from menu.models import Dish

class CurrentDayOrdersView(ListView):
    template_name = u"statistics/current-day-orders.html"
    queryset = Order.objects.filter(
        date__date=datetime.datetime.now().date()
    )
    context_object_name = u"orders"

    def get_context_data(self, **kwargs):
        context = super(CurrentDayOrdersView, self).get_context_data(**kwargs)
        current_day_orders = self.get_queryset()

        context["total_sum"] = current_day_orders.aggregate(
                cost_sum = Sum("cost")
            )["cost_sum"] or 0
        context["orders_amount"] = current_day_orders.count()
        return context

class GeneralStatistics(ListView):
    model = Order
    template_name = u"statistics/general-statistics.html"
    context_object_name = u"orders"
    months_in_year = 12

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(GeneralStatistics, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        current_month = datetime.datetime.now().month
        current_year = datetime.datetime.now().year
        first_day, days_in_month = calendar.monthrange(current_year,
                                                        current_month)
        context = super(GeneralStatistics, self).get_context_data(**kwargs)
        context["statistics_for_days"] = self.get_statistics_for_days(
            current_month,
            current_year
        )
        context["statistics_for_months"] = self.get_statistics_for_months(
            current_month,
            current_year
        )
        context["dishes_per_day"] = self.get_statistics_for_dishes_per_day(
            current_month,
            current_year
        )
        context["dishes_per_month"] = self.get_statistics_for_dishes_per_month(
            current_month,
            current_year
        )
        context["day_range"] = range(1, days_in_month + 1)
        context["days_in_month"] = days_in_month
        context["month_range"] = range(1, self.months_in_year + 1)
        context["months_in_year"] = self.months_in_year
        return context

    def get_statistics_for_days(self, month, year):
        first_day, days_in_month  = calendar.monthrange(year, month)
        statistics_for_days = []
        for day_number in range(1, days_in_month + 1):
            day_orders = Order.objects.filter(
                date__year = year,
                date__month = month,
                date__day = day_number
            )
            statistics = {
                "number_of_orders": day_orders.count(),
                "total_cost": (day_orders.aggregate(
                        total_cost = Sum("cost")
                    )["total_cost"] or 0)
            }
            statistics_for_days.append(statistics)
        return statistics_for_days

    def get_statistics_for_months(self, month, year):
        statistics_for_months = []
        for month in range(1, self.months_in_year + 1):
            month_orders = Order.objects.filter(
                date__year = year,
                date__month = month
            )
            statistics = {
                "number_of_orders": month_orders.count(),
                "total_cost": (month_orders.aggregate(
                        total_cost = Sum("cost")
                    )["total_cost"] or 0)
            }
            statistics_for_months.append(statistics)
        return statistics_for_months

    def get_statistics_for_dishes_per_day(self, month, year):
        dishes = Dish.objects.all()
        first_day, days_in_month  = calendar.monthrange(year, month)

        dishes_per_day = {}
        for dish in Dish.objects.all():
            dishes_per_day[dish.name] = {
                day: 0 for day in range(1, days_in_month + 1)
            }
        month_orders = Order.objects.filter(
            date__year = year,
            date__month = month,
        )
        for day in range(1, days_in_month + 1):
            day_orders = month_orders.filter(
                date__day = day
            )
            for order in day_orders:
                day_order_items = OrderItem.objects.filter(order = order)
                for item in day_order_items:
                    dish_name = item.dish.name
                    dish_dictionary = dishes_per_day.get(dish_name)
                    if dish_dictionary:
                        dish_dictionary[day] += item.quantity
        return dishes_per_day

    def get_statistics_for_dishes_per_month(self, month, year):
        dishes = Dish.objects.all()
        dishes_per_month = {}
        for dish in Dish.objects.all():
            dishes_per_month[dish.name] = {
                month: 0 for month in range(1, self.months_in_year + 1)
            }
        year_orders = Order.objects.filter(
            date__year = year,
        )
        for month in range(1, self.months_in_year + 1):
            month_orders = year_orders.filter(
                date__month = month
            )
            for order in month_orders:
                month_order_items = OrderItem.objects.filter(order = order)
                for item in month_order_items:
                    dish_name = item.dish.name
                    dish_dictionary = dishes_per_month.get(dish_name)
                    if dish_dictionary:
                        dish_dictionary[month] += item.quantity
        return dishes_per_month
