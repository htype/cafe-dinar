import csv
import io
import os
import shutil
import filecmp
from urlparse import urljoin
from xml.etree import ElementTree

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from menu.models import Category, Dish


class Command(BaseCommand):
    dish_images_directory = "images/dishes/"
    dish_images_url = "images/dishes/"
    source_image_directory = "images"

    def add_arguments(self, parser):
        parser.add_argument(
            "--file",
            required=True,
            help="CSV file to import"
        )

    def import_from_csv(self, file_name, source_directory):
        csv_file = io.open(file_name, "r", encoding="utf-8")
        for line_number, current_string in enumerate(csv_file):
            # skip header with columns names
            if line_number == 0:
                continue

            list_of_tokens = current_string.split(";")
            number_of_elements = len(list_of_tokens)
            if number_of_elements != 4:
                raise CommandError(u"wrong number of elements in row ({})"
                                    .format(number_of_elements))
            [dish_name, category_name, cost, image_file_name] = list_of_tokens
            error = self.update_or_create_dish(dish_name, category_name, cost,
                                        image_file_name, source_directory)
            if error:
                raise CommandError(u"Error on line {}: {}"
                                    .format(line_number+1, error))

    def extract_attribute(self, attribute_name, dish_tag, tag_number):
        attribute = dish_tag.attrib.get(attribute_name)
        if not attribute:
            raise CommandError(u"There is no attribute '{}' in tag '{}' ({})"
                            .format(attribute_name, dish_tag.tag, tag_number))
        return attribute

    def import_from_xml(self, file_name, source_directory):
        etree = ElementTree.parse(file_name)
        dish_tags =  etree.findall('dish')
        for tag_number, tag in enumerate(dish_tags):
            dish_name      = self.extract_attribute("name", tag, tag_number)
            category_name  = self.extract_attribute("category", tag, tag_number)
            cost           = self.extract_attribute("cost", tag, tag_number)
            image_file_name = self.extract_attribute("image", tag, tag_number)
            error = self.update_or_create_dish(dish_name, category_name, cost,
                                            image_file_name, source_directory)
            if error:
                raise CommandError(u"Error on tag: {}"
                                .format(line_number+1, error))


    def handle(self, *args, **options):
        menu_file_name = options["file"].strip()
        source_directory = os.path.join(os.path.dirname(menu_file_name),
                                        self.source_image_directory)
        extension = menu_file_name.split(".")[-1]
        if extension == "csv":
            self.import_from_csv(menu_file_name, source_directory)
        elif extension == "xml":
            self.import_from_xml(menu_file_name, source_directory)
        else:
            raise CommandError(u'''Invalid file extension. Command import_menu
                                support .xml and .csv files.''')

    def update_or_create_dish(self, dish_name, category_name, cost,
                            image_file_name, source_directory):
        category, created = Category.objects.update_or_create(
            name=category_name
        )
        try:
            dish_cost = int(cost)
        except ValueError:
            return u"invalid cost {}".format(cost)
        clean_image_file_name = image_file_name.strip()
        image_url = urljoin(self.dish_images_url, clean_image_file_name)

        dish, created = Dish.objects.update_or_create(
            name     = dish_name,
            defaults = {
                "category": category,
                "cost":     dish_cost,
                "image":    image_url,
            },
        )
        self.copy_dish_image_file(clean_image_file_name, source_directory)
        return None


    def copy_dish_image_file(self, file_name, source_directory):
        new_image_path = os.path.join(settings.MEDIA_ROOT,
                                    self.dish_images_directory, file_name)
        source_image_path = os.path.join(source_directory, file_name)
        target_image_directory = os.path.dirname(new_image_path)

        if (not os.path.exists(target_image_directory)):
            os.makedirs(target_image_directory)

        if (not os.path.exists(source_image_path)):
            raise CommandError(u"There is no {} file in source directory."
                            .format(source_image_path))

        if (not os.path.exists(new_image_path) or
            not filecmp.cmp(new_image_path, source_image_path)):
            shutil.copy(source_image_path, new_image_path)
