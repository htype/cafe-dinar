#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms

from menu.models import Dish, Category

class DishAdminModelForm(forms.ModelForm):
    class Meta:
        model = Dish
        fields = ["name", "cost", "image", "category"]
        widgets = {
            "category": forms.TextInput()
        }
