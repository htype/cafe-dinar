import calendar
import xlwt
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError

from statistics.views import GeneralStatistics

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            u"--output",
            u"-o",
            required=True,
            help="Name of file to export"
        )

    def handle(self, *args, **options):
        output_file_name = options[u"output"].strip()
        current_month = datetime.now().month
        current_year = datetime.now().year

        statistics = GeneralStatistics()
        statistics_for_days = statistics.get_statistics_for_days(
            current_month,
            current_year
        )
        statistics_for_months = statistics.get_statistics_for_months(
            current_month,
            current_year
        )
        dishes_per_day = statistics.get_statistics_for_dishes_per_day(
            current_month,
            current_year
        )
        dishes_per_month = statistics.get_statistics_for_dishes_per_month(
            current_month,
            current_year
        )

        workbook = xlwt.Workbook()
        self.addOrderStatisticsToXls(workbook, statistics_for_days, "days")
        self.addOrderStatisticsToXls(workbook, statistics_for_months, "months")
        self.addDishStatisticsToXls(workbook, dishes_per_day, "days")
        self.addDishStatisticsToXls(workbook, dishes_per_month, "months")

        workbook.save(output_file_name)

    def addOrderStatisticsToXls(self, workbook, statistics, time_unit_title):
        sheet_title = "Statistics for " + time_unit_title
        statistics_sheet = workbook.add_sheet(sheet_title)

        # Title
        statistics_sheet.write(0, 0, time_unit_title)
        statistics_sheet.write(0, 1, "Total cost")
        statistics_sheet.write(0, 2, "Number of orders")

        # Content
        for index, statistics_item in enumerate(statistics):
            time_unit = index + 1
            statistics_sheet.write(time_unit, 0, time_unit)
            statistics_sheet.write(
                time_unit, 1, statistics_item["total_cost"]
            )
            statistics_sheet.write(
                time_unit, 2, statistics_item["number_of_orders"]
            )

    def addDishStatisticsToXls(self, workbook, statistics, time_unit_title):
        sheet_title = "Dish statistics for " + time_unit_title
        statistics_sheet = workbook.add_sheet(sheet_title)

        # Title
        time_units_count = len(statistics.items()[0][1])
        statistics_sheet.write_merge(0, 1, 0, 0, "dish")
        statistics_sheet.write_merge(0, 0, 1, time_units_count, time_unit_title)
        for index in range(time_units_count):
            statistics_sheet.write(1, index + 1, index + 1)

        # Content
        row = 2
        for dish_name in statistics:
            statistics_sheet.write(row, 0, dish_name)
            quantity_per_day = statistics[dish_name]
            for index, day in enumerate(quantity_per_day):
                statistics_sheet.write(row, index + 1, quantity_per_day[day])
            row += 1
