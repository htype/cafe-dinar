#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms

from orders.models import Client, Order, OrderItem


class ClientAdminModelForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ["user"]
        widgets = {
            "user": forms.TextInput()
        }


class OrderAdminModelForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ["client", "dishes", "cost", "status"]
        widgets = {
            "client": forms.TextInput(),
        }

class OrderItemAdminModelForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = ["order", "dish", "quantity"]
        widgets = {
            "order": forms.TextInput(),
            "dish": forms.TextInput(),
        }
