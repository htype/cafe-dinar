#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models

class Category(models.Model):
    class Meta:
        verbose_name = u"Категория"
        verbose_name_plural = u"Категории"

    name = models.CharField(verbose_name=u"название", max_length=200, unique=True)

    def __unicode__(self):
        return self.name

class Dish(models.Model):
    class Meta:
        verbose_name = u"Блюдо"
        verbose_name_plural = u"Блюда"

    category = models.ForeignKey(Category, verbose_name=u"категория", on_delete=models.CASCADE)
    name = models.CharField(verbose_name=u"название", max_length=200, unique=True)
    cost = models.PositiveIntegerField(verbose_name=u"стоимость")
    image = models.ImageField(verbose_name=u"изображение", blank=True, null=True)

    def __unicode__(self):
        return self.name
