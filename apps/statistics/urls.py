from django.conf.urls import url

from statistics.views import CurrentDayOrdersView, GeneralStatistics

urlpatterns = [
    url(r'^current-day-orders/', CurrentDayOrdersView.as_view()),
    url(r'^$', GeneralStatistics.as_view()),
]
