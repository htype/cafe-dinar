(function() {
    google.charts.load('current', {'packages':['corechart']});

    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = new google.visualization.DataTable();
        var totalCostDataItems = $(".total-cost-data-item");
        var totalCostValues = $.map(totalCostDataItems, (function(value, key) {
            var costAsInt = parseInt($(value).text())
            var day = (key + 1).toString()
            return [[day, costAsInt]];
        }));
        data.addColumn('string', 'Day');
        data.addColumn('number', 'Total cost');
        data.addRows(totalCostValues);

        number_of_items = totalCostValues.length;
        var options = {
            'title': 'Total cost of orders per day',
            'width': 800,
            'height': 400,
            'hAxis': { title: "Day" },
            'vAxis': { title: "Total cost" }
        };

        var chart = new google.visualization.ScatterChart($('.order-total-sum-graph')[0]);
        chart.draw(data, options);
    }
})()
