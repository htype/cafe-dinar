from django.conf.urls import url

from orders.views import BasketUpdateItem, BasketDeleteAll
from orders.views import ChequeView

urlpatterns = [
    url(r'^basket/update-item/$', BasketUpdateItem.as_view()),
    url(r'^basket/delete-all/$', BasketDeleteAll.as_view()),
    url(r'(?P<pk>\d+)/$', ChequeView.as_view(), name=u"cheque_url"),
]
